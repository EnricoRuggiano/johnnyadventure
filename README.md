# JohnnyAdventure
Web Game

## Hot to install
- `git clone` this repo
- `npm install`
- put your assets files in `www/assets`

## How to Build
### Browser
- `npm run build`

### Android
- `npm run build-android`

## How to Play
### Localhost
- `npm start`
-  connect to `localhost:8000/www`

### Android
-  cd `platforms/android/app/build/outputs/apk/debug`
-  copy to your android `app-debug.apk`

# Requirements

- `npm`
- `phaser 3`
- `webpack`
- `cordova` with [android](https://cordova.apache.org/docs/en/latest/guide/platforms/android/)
